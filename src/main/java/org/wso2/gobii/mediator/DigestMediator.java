package org.wso2.gobii.mediator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.synapse.MessageContext;
import org.apache.synapse.core.axis2.Axis2MessageContext;
import org.apache.synapse.mediators.AbstractMediator;
import org.gobii.bert.Components;
import org.gobii.bert.shell.Shell;
import org.gobii.bert.shell.Ssh;

public class DigestMediator extends AbstractMediator { 
	
public String message;

	public boolean mediate(MessageContext context) { 
           //Aspect - json document describing the transform
        String digestAspect = getStringProp(context,"Aspect");
        String computeNodePath = getStringProp(context,"ComputePath");
        String computeUser = getStringProp(context,"ComputeUser");
        String computePass = getStringProp(context,"ComputePass");
        String inputFilePath = getStringProp(context,"InputFilePath");
        
        /* //These may exist in the future for element creation in the DB
        String contact = getStringProp(context,"Contact");
        String project = getStringProp(context,"Project");
        String experiment = getStringProp(context,"Experiment");
        String platform = getStringProp(context,"Platform");
        String dataset = getStringProp(context,"Dataset");
        String mapset = getStringProp(context,"Mapset");
         */

        //Path to HDF5 File Output Location
        String hdfOutputLocation = getStringProp(context,"HDFOutputLocation");
        //Size of HDF5 Elements, hopefully can be removed at some point
        String hdfDataSize = getStringProp(context,"HDFDataSize");
        //For database connection
        String databaseConnectionString=getStringProp(context,"DatabaseConnectionString");
        
        
        
        LoadType loadType = getLoadType(digestAspect);
        //TODO - based on load type, make sure all correct info is passed, for now this is double placebo
        
        
        boolean success= callDigest(digestAspect,inputFilePath,computeNodePath,computeUser,computePass, databaseConnectionString, hdfOutputLocation, hdfDataSize);
        
        return success; 
    }
	
	private static String getStringProp(MessageContext c, String propName) {
        return(String) c.getProperty(propName);

	}
	
	/**
	 * Calls digest process
	 * @param digestAspect String containing json-encoded aspect data
	 * @param dataFile Location of input data file
	 * @param computeNodePath Location of the gobii compute node
	 * @param databaseURL URL of the gobii database
	 * @param hdf5OutputLocation File Location of HDF5 output file (if this is a dataset load)
	 * @param hdfDataSize Size, in characters, of the maximum width of the output of the dataset (if this is a dataset load)
	 * @return If the connection to the compute node succeeded
	 */
	private static boolean callDigest(String digestAspect, String dataFile, String computeNodePath, String computeUser, String computePass, String databaseURL, String hdf5OutputLocation, String hdfDataSize) {
		//TODO - with time, this can be replaced with spinning up a docker image on the compute node
		
		
		//Current impl - rely heavily on BERT's ability to ssh into nodes.


		//ASSUMES 5 ARGs in order given in slack for EBS_GOBII
		Shell shell = new Ssh(computeNodePath,computeUser,computePass);
		String execString = String.format("(cd /data/gobii_bundle/core && java -Dorg.gobii.environment.name=BERT -jar Digester.jar %s %s %s %s %s)", digestAspect,dataFile,databaseURL,hdf5OutputLocation,hdfDataSize);
		try{
			shell.execute(execString);
			return true;//Assume job submitted and ran successfully
		}
		catch(Exception e) {
		return false; //Something went wrong, fail
		}
	}
	
	private static LoadType getLoadType(String aspect) {
		//AspectObject aspect = new ObjectMapper().readValue(str, AspectObject.class)
		
		return LoadType.MATRIX;
	}
    public String getMessage() {
    	return message;
    }
    public void setMessage(String messages) {
    	this.message = messages;
    }
    
}
enum LoadType{MARKER,SAMPLE,MATRIX};